package com.tinyurl.controller;

import com.google.common.hash.Hashing;
import com.tinyurl.model.Error;
import com.tinyurl.model.Url;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;


@RestController
@RequestMapping(value = "/rest/url")
@Api(value = "Url Shortener Api Documentation")
public class UrlShortenerController {

    @Autowired
    RedisTemplate<String,Url> redisTemplate;

    @Value("${redis.ttl}")
    private long ttl;

    @PostMapping
    @ResponseBody
    @ApiOperation(value = "Create New Tiny Url Method")
    public ResponseEntity postTinyUrl(@RequestBody Url url){
        UrlValidator urlValidator=new UrlValidator(new String[]{"http","https"});

        if (!urlValidator.isValid(url.getUrl())){
            Error error=new Error("url",url.getUrl(),"Invalid URL");
            return ResponseEntity.badRequest().body(error);
        }
        String id = Hashing.murmur3_32().hashString(url.getUrl(), Charset.defaultCharset()).toString();
        url.setId(id);
        url.setLocalDateTime(LocalDateTime.now());

        redisTemplate.opsForValue().set(url.getId(),url,ttl, TimeUnit.SECONDS);

        return ResponseEntity.ok(url);
    }

    @GetMapping(value = "/{id}")
    @ApiOperation(value = "Relocate to Real Url Method")
    public  ResponseEntity getUrl(@PathVariable String id){
        Url url=redisTemplate.opsForValue().get(id);

        if (url==null){
            Error error=new Error("id",id,"No such key exists");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
        }

        return ResponseEntity.status(HttpStatus.FOUND).location(URI.create(url.getUrl())).build();
    }

}

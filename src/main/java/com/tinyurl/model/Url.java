package com.tinyurl.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;

@ApiModel(value = "Url Api model documentation")
public class Url {
    @ApiModelProperty
    String id;
    @ApiModelProperty
    String url;
    @ApiModelProperty
    LocalDateTime localDateTime;

    public Url(String id, String url, LocalDateTime localDateTime) {
        this.id = id;
        this.url = url;
        this.localDateTime = localDateTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }
}
